class Holiday {

    constructor(name,date){
        this.name = name;
        this.date = date;
    }

    getDate(){
        return this.date;
    }
}

module.exports = Holiday;