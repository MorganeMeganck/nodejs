/**
 * 
 * @param {number} date  journée du mois
 * @param {number} month valeur du mois (de 1 à 12)
 * @returns {Date} la prochaine date
 */

const getNextEventDate = (date,month) => {
    const today = new Date();
    let year = today.getFullYear();
    const indexMonth = month -1;
    if(today.getMonth() > indexMonth || (today.getMonth() === indexMonth && today.getDate() > date)){
        year++;
    }
    return new Date(year, indexMonth, date);
}

const getDiffdays = (targetDate) => {
    const today = new Date();
    const diff = targetDate.getTime() - today.getTime();
    const oneDay = 1000*60*60*24;
    return Math.ceil(diff/oneDay);
}


const eventDays = {
    fromChristmas: function(){
        const nextChristmas = getNextEventDate(25,12);
        const nbDiffDays = getDiffdays(nextChristmas);
        return nbDiffDays;
    },
    fromBirthdate: function(date,month){
        const nextBirthdate = getNextEventDate(date, month);
        return getDiffdays(nextBirthdate);
    },
    fromHolidays: () => {
        const month = (new Date().getMonth());
        if(month >=6 && month <=7){
            return 0
        }
        const nextBeginHolidays = getNextEventDate(1,7);
        return getDiffdays(nextBeginHolidays);
    },
    fromSolstice: () =>{
        const nextSummerSolstice = getNextEventDate(21,6);
        const nextWinterSolstice = getNextEventDate(21,12);

        const nbSummerDays = getDiffdays(nextSummerSolstice);
        const nbWinterDays = getDiffdays(nextWinterSolstice);
        console.log(nbSummerDays);
        console.log(nbWinterDays);

        // return (nbSummerDays < nbWinterDays) ? nbSummerDays : nbWinterDays;
        return Math.min(nbSummerDays,nbWinterDays);
    },
    fromFridayThirtheen: () => { //setDate méthode "facile"
        const today = new Date();
        // permet d'obtenir le prochain mois ou il y a un 13e jour
        let targetMonth = today.getMonth();
        if (today.getDate() >= 13){
            targetMonth++;
        }
        // obtenir le prochain 13 du mois
        const nextFriday = new Date(today.getFullYear(), indexMonth,13)

        // test si c'est un vendredi -> Si c'est pas le cas, passe au mois suivant
        while(nextFriday.getDay() !== 5){
            // on passe au mois suivant
            nextFriday.setMonth(nextFriday.getMonth() + 1);
        }
        return getDiffdays(nextFriday);
    }
}
module.exports = eventDays;